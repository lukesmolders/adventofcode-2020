using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace adventofcode_2020
{
    class Part06_1
    {
        public static void Run()
        {
            string[] inputFromTXT = System.IO.File.ReadAllLines("./part06.1/input.txt");
            int total = 0;

            List<char> answersFromGroup = new List<char>();

            // Check each row
            foreach (string answerFromPerson in inputFromTXT)
            {
                // check if its not a empty row
                if (answerFromPerson.Length > 0)
                {
                    foreach (char answer in answerFromPerson)
                    {
                        if (!answersFromGroup.Contains(answer))
                        {
                            answersFromGroup.Add(answer);
                            total++;
                        }
                    }
                }
                // if its a empty row
                else {
                    answersFromGroup.Clear();
                }
            }

            Console.WriteLine($"Total answers is: {total}");



        }
    }
}
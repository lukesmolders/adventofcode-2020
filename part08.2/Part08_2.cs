using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace adventofcode_2020
{
    class Part08_2
    {
        public static void Run()
        {
            string[] inputFromTXT = System.IO.File.ReadAllLines("./part08.2/input.txt");
            List<Instructionv2> instructionList = new List<Instructionv2>();


            foreach (string Instruction in inputFromTXT)
            {
                string[] split = Instruction.Split(" ");
                instructionList.Add(new Instructionv2(split[0], int.Parse(split[1])));
            }

            int accumulator = 0;
            int currentposition = 0;

            for (int i = 0; i < instructionList.ToList().Count; i++)
            {
                accumulator = 0;
                currentposition = 0;

                instructionList.Clear();

                foreach (string Instruction in inputFromTXT)
                {
                    string[] split = Instruction.Split(" ");
                    instructionList.Add(new Instructionv2(split[0], int.Parse(split[1])));
                }

                Instructionv2 currentInstruction = instructionList[currentposition];

                switch (instructionList[i].InstructionType)
                {
                    case "nop":
                        instructionList[i] = new Instructionv2("jmp", instructionList[i].amount);
                        break;
                    case "jmp":
                        instructionList[i] = new Instructionv2("nop", instructionList[i].amount);
                        break;
                }

                while (true)
                {
                    currentInstruction.timesRan++;

                    if (currentInstruction.timesRan > 1)
                    {
                        break;
                    }

                    switch (currentInstruction.InstructionType)
                    {
                        case "nop":
                            currentposition++;
                            break;
                        case "acc":
                            accumulator += currentInstruction.amount;
                            currentposition++;
                            break;
                        case "jmp":
                            currentposition += currentInstruction.amount;
                            break;
                    }

                    if(currentposition >= instructionList.Count) {
                        Console.WriteLine($"The accumulator is at position: {accumulator}");
                        break;
                    }

                    currentInstruction = instructionList[currentposition];

                }
            }

         


        }

    }

    class Instructionv2
    {

        /*
        acc - increases
        jmp - jump to new instruction
        nop - nothing
        */
        public string InstructionType;
        public int amount;
        public int timesRan = 0;

        public Instructionv2(string instructionType, int amount)
        {
            InstructionType = instructionType;
            this.amount = amount;
        }
    }

}
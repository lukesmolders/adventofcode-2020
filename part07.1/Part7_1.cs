using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace adventofcode_2020
{
    class Part07_1
    {
        public static void Run()
        {
            string[] inputFromTXT = System.IO.File.ReadAllLines("./part07.1/input.txt");
            List<string> mainbags = new List<string>();

            // Mainbags with gold
            foreach (string row in inputFromTXT)
            {
                var filterRow = row.Split("contain")[0];
                if (row.Contains("shiny gold bag"))
                {
                    mainbags.Add(filterRow.Substring(0, filterRow.Length - 2));
                }
            }

            int oldTotal = 0;
            int newTotal = 0;

            List<string> newBagList = inputFromTXT.ToList();
            do
            {
                oldTotal = newTotal;

                foreach (string bag in mainbags.ToList())
                {
                    foreach (string row in inputFromTXT)
                    {
                        var filterRow = row.Split("contain")[1];
                        var thisBag = bag.Substring(0, bag.Length - 2);
                        if (filterRow.Contains(thisBag))
                        {
                            var splitRow = row.Split("contain")[0];
                            var toAdd = splitRow.Substring(0, splitRow.Length - 2);
                            if(!mainbags.Contains(toAdd)) mainbags.Add(toAdd);
                        }
                    }
                }

                newTotal = mainbags.Count();

            } while (oldTotal != newTotal);

            Console.WriteLine(newTotal - 1);
        }
    }

}
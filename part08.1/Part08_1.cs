using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace adventofcode_2020
{
    class Part08_1
    {
        public static void Run()
        {
            string[] inputFromTXT = System.IO.File.ReadAllLines("./part08.1/input.txt");
            List<Instruction> instructionList = new List<Instruction>();

            foreach (string Instruction in inputFromTXT)
            {
                string[] split = Instruction.Split(" ");
                instructionList.Add(new Instruction(split[0], int.Parse(split[1])));
            }

            int accumulator = 0;
            int currentposition = 0;
            Instruction currentInstruction = instructionList[currentposition];

            while (true)
            {
                currentInstruction.timesRan++;
                
                if(currentInstruction.timesRan > 1) {
                    break;
                }

                switch (currentInstruction.InstructionType)
                {
                    case "nop":
                        currentposition++;
                        currentInstruction = instructionList[currentposition];
                        break;
                    case "acc":
                        accumulator += currentInstruction.amount;
                        currentposition++;
                        currentInstruction = instructionList[currentposition];
                    break;
                    case "jmp":
                        currentposition += currentInstruction.amount;
                        currentInstruction = instructionList[currentposition];
                        break;
                }


            }

            Console.WriteLine($"The accumulator is at position: {accumulator}");


        }

    }

    class Instruction
    {

        /*
        acc - increases
        jmp - jump to new instruction
        nop - nothing
        */
        public string InstructionType;
        public int amount;
        public int timesRan = 0;

        public Instruction(string instructionType, int amount)
        {
            InstructionType = instructionType;
            this.amount = amount;
        }
    }

}
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace adventofcode_2020
{
    class Part09_2
    {
        public static void Run()
        {
            string[] inputFromTXT = System.IO.File.ReadAllLines("./part09.2/input.txt");
            List<Int64> intList = new List<Int64>();


            foreach (string number in inputFromTXT)
            {
                intList.Add(Int64.Parse(number));
            }


            const int searchingFor = 1504371145;
            //const int searchingFor = 127;

            int startingPosition = 0;
            int rangeLength = 1;
            Int64 total = 0;
            List<Int64> rangeFromlist = new List<Int64>();

            List<Int64> succesRange = new List<Int64>();

            while(true) {
                rangeLength++;
                total = 0;
                rangeFromlist.Clear();
                rangeFromlist = intList.GetRange(startingPosition, rangeLength);

                foreach (Int64 itemFromList in rangeFromlist)
                {
                    total += itemFromList;
                }

                if (total == searchingFor)
                {
                    succesRange = rangeFromlist;
                    break;
                }

                if (total > searchingFor)
                {
                    startingPosition += 1;
                    rangeLength = 0;
                }

            }

            succesRange.Sort();
            foreach (var item in succesRange)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine($"Total min and max is {succesRange.First() + succesRange.Last()}");

        }



    }

}
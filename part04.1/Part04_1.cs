﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode_2020
{
    class part04_1
    {
        public static void Run()
        {
            Console.WriteLine("Hello World!");
            string[] inputFromTXT = System.IO.File.ReadAllLines("input.txt");


            List<PassPort> possiblePassports = new List<PassPort>();

            PassPort tempPass = new PassPort();
            var i = 1;
            foreach (var item in inputFromTXT)
            {
                if (item == "")
                {
                    possiblePassports.Add(tempPass);
                    tempPass = null;
                    tempPass = new PassPort();
                }
                else
                {
                    string[] passPortProperties = item.Split(" ");
                    foreach (var property in passPortProperties)
                    {
                        string[] keyValue = property.Split(":");
                        tempPass.PassPortData.Add(keyValue[0], keyValue[1]);

                    }

                    if (i == inputFromTXT.Length)
                    {
                        possiblePassports.Add(tempPass);
                    }
                }

                i++;

            }

            int totalValidPassports = 0;
            foreach (var couldbePassport in possiblePassports)
            {
                if(couldbePassport.PassPortData.Count == 8) totalValidPassports++;
                if(couldbePassport.PassPortData.Count == 7) {
                    if(!couldbePassport.PassPortData.ContainsKey("cid")) totalValidPassports++;
                }
            }

            Console.WriteLine($"Total validPassports are: {totalValidPassports}");

        }
    }

    enum PassPortField
    {
        byr, iyr, eyr, hgt, hcl, ecl, pid, cid
    }

}

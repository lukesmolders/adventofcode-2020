﻿using System;

namespace adventofcode_2020
{
    class Program
    {
        static void Main(string[] args)
        {
            Part09_2.Run();
        }
    }
}

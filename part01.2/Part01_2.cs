﻿using System;
using System.Linq;

namespace adventofcode_2020
{
    class part01_2
    {
        public static void Run()
        {
            string[] inputFromTXT = System.IO.File.ReadAllLines("input.txt");
            int[] collection = inputFromTXT.Select(int.Parse).ToArray();

            int key1 = 0;
            int key2 = 0;
            int key3 = 0;
            bool itsdone = false;

            foreach (int number1 in collection)
            {
                if (itsdone) break;
                key1 = number1;

                foreach (int number2 in collection)
                {
                    if (itsdone) break;
                    key2 = number2;

                    foreach (int number3 in collection)
                    {
                        key3 = number3;
                        int total = key1 + key2 + key3;

                        if (total == 2020)
                        {
                            itsdone = true;
                            break;
                        }
                    }

                }

            }

            int sum = key1 * key2 * key3;
            Console.WriteLine($"{key1} and {key2} and {key3}");
            Console.WriteLine(sum);
        }
    }
}

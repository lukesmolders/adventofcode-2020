﻿using System;
using System.Linq;

namespace adventofcode_2020
{
    class part01_1
    {
        public static void Run()
        {
            string[] inputFromTXT = System.IO.File.ReadAllLines("input.txt");
            int[] collection = inputFromTXT.Select(int.Parse).ToArray();

            int key1 = 0;
            int key2 = 0;
            bool itsdone = false;
            
            foreach (int number1 in collection)
            {
                if(itsdone) break;
                key1 = number1;

                foreach (int number2 in collection)
                {
                    key2 = number2;
                    int total = key1 + key2;
                    

                    if(total == 2020) {
                        itsdone = true;
                        break;
                    }
                }

            }

            int sum = key1 * key2;
            Console.WriteLine($"{key1} and {key2}");
            Console.WriteLine(sum);
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace adventofcode_2020
{
    class Part02_1
    {
        public static void Run()
        {
            string[] inputFromTXT = System.IO.File.ReadAllLines("input.txt");
            int validCounter = 0;        

            //key -> rules
            //value -> password
            Dictionary<List<string>, string> passwordDB = new Dictionary<List<string>, string>();

            foreach (string item in inputFromTXT)
            {
                List<string> list = new List<string>();
                string[] seperate = item.Split(" ");
                string[] seperateNumbers = seperate[0].Split("-");
                // Add numbersrule
                list.Add(seperateNumbers[0]);
                list.Add(seperateNumbers[1]);
                // Add letter rule
                list.Add(seperate[1].Split(":")[0]);
                // Add letter rule
                passwordDB.Add(list, seperate[2]);
            }

            foreach (KeyValuePair<List<string>, string> item in passwordDB)
            {
                int lowestAmount = int.Parse(item.Key[0]);
                int highestAmount = int.Parse(item.Key[1]);
                int count = item.Value.Length - item.Value.Replace(item.Key[2], "").Length;

                if(count >= lowestAmount && count <= highestAmount) {
                    validCounter++;
                }
            }

            Console.WriteLine($"the amount of valid passwords are : {validCounter}");
        }
    }
}

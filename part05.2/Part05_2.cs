﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace adventofcode_2020
{
    class Part05_2
    {
        public static void Run()
        {
            string[] inputFromTXT = System.IO.File.ReadAllLines("./part05.2/input.txt");
            List<int> allSeats = new List<int>();

            foreach (string ticket in inputFromTXT)
            {
                string currentRow = ticket.Substring(0, 7).Replace("F", "0").Replace("B", "1");
                string currentColumn = ticket.Substring(7).Replace("L", "0").Replace("R", "1"); ;
                int rowNumber = Convert.ToInt16(currentRow, 2);
                int columnNumer = Convert.ToInt16(currentColumn, 2);

                int seatPosition = (rowNumber * (8)) + columnNumer;
                allSeats.Add(seatPosition);

            }
            allSeats.Sort();
            int previousnumber = allSeats.Min() - 1;
            foreach (int item in allSeats)
            {
                if (item - 1 != previousnumber) {
                    Console.WriteLine(item - 1);
                    break;
                }
                previousnumber++;
            }
        }
    }
}

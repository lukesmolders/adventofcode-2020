using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace adventofcode_2020
{
    class PassPort {
        public Dictionary<string, string> PassPortData = new Dictionary<string, string>();

        public bool byr(string byr) {
            if (byr.Length != 4) return false;
            
            int numberByr = int.Parse(byr);

            if(numberByr < 1920 || numberByr > 2002) return false;

            return true;
        }

        public bool iyr(string iyr) {
            if (iyr.Length != 4) return false;

            int numberIyr = int.Parse(iyr);

            if (numberIyr < 2010 || numberIyr > 2020) return false;

            return true;
        }

        public bool eyr(string eyr) {
            if (eyr.Length != 4) return false;

            int numberEyr = int.Parse(eyr);

            if (numberEyr < 2020 || numberEyr > 2030) return false;

            return true;
        }

        public bool ecl(string ecl) {
            string[] haircolorList = {"amb", "blu", "brn", "gry", "grn", "hzl", "ot"};

            if (ecl.Length > 3) return false;

            if(haircolorList.Contains(ecl)) return true; 
            else return false;
        }

        public bool pid(string pid) {
            if(pid.Length != 9) return false;
            //if(pid[0] != '0') return false;

            return true;
        }

        public bool hgt(string hgt) {
            if(hgt.Contains("cm")) {
                hgt = hgt.Replace("cm", "");
                int makeToInt = int.Parse(hgt);

                return makeToInt >= 150 && makeToInt <= 193;
            }
            else if(hgt.Contains("in")) {
                hgt = hgt.Replace("in", "");
                int makeToInt = int.Parse(hgt);

                return makeToInt >= 59 && makeToInt <= 76;
            }
            
            return false;
        }

        public bool hcl(string hcl) {
            if (hcl.Length != 7) return false;
            if (hcl[0] != '#') return false;

            Regex rg = new Regex("^#[a-f0-9]{6}$");
            return rg.IsMatch(hcl);
        }
    }


}

enum PassPortField
{
    byr, iyr, eyr, hgt, hcl, ecl, pid, cid
}

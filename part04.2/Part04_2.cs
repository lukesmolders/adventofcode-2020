﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace adventofcode_2020
{
    class part04_2
    {
        public static void Run()
        {
            Console.WriteLine("Hello World!");
            string[] inputFromTXT = System.IO.File.ReadAllLines("input.txt");


            List<PassPort> possiblePassports = new List<PassPort>();

            PassPort tempPass = new PassPort();
            var i = 1;
            foreach (var item in inputFromTXT)
            {
                if (item == "")
                {
                    possiblePassports.Add(tempPass);
                    tempPass = null;
                    tempPass = new PassPort();
                }
                else
                {
                    string[] passPortProperties = item.Split(" ");
                    foreach (var property in passPortProperties)
                    {
                        string[] keyValue = property.Split(":");
                        tempPass.PassPortData.Add(keyValue[0], keyValue[1]);

                    }

                    if (i == inputFromTXT.Length)
                    {
                        possiblePassports.Add(tempPass);
                    }
                }

                i++;

            }

            List<PassPort> AttributeValidPassports = new List<PassPort>();
            foreach (var couldbePassport in possiblePassports)
            {
                if(couldbePassport.PassPortData.Count == 8) AttributeValidPassports.Add(couldbePassport);
                if(couldbePassport.PassPortData.Count == 7) {
                    if(!couldbePassport.PassPortData.ContainsKey("cid")) AttributeValidPassports.Add(couldbePassport);
                }
            }


            int totalValidPassports = 0;
            foreach (var couldbePassport in AttributeValidPassports) {
                int passedTests = 0;

                var test = couldbePassport.PassPortData["byr"];

                if(couldbePassport.byr(couldbePassport.PassPortData["byr"])) {
                    passedTests++;
                }
                if(couldbePassport.iyr(couldbePassport.PassPortData["iyr"])) {
                    passedTests++;
                }
                if(couldbePassport.eyr(couldbePassport.PassPortData["eyr"])) {
                    passedTests++;
                }
                if(couldbePassport.hgt(couldbePassport.PassPortData["hgt"])) {
                    passedTests++;
                }
                if(couldbePassport.hcl(couldbePassport.PassPortData["hcl"])) {
                    passedTests++;
                }
                if(couldbePassport.ecl(couldbePassport.PassPortData["ecl"])) {
                    passedTests++;
                }
                if(couldbePassport.pid(couldbePassport.PassPortData["pid"])) {
                    passedTests++;
                }
                
                if(passedTests == 7) {
                    totalValidPassports++;
                }
            }
            Console.WriteLine($"Total validPassports are: {totalValidPassports}");
        }
    }

}

﻿using System;

namespace adventofcode_2020
{
    class part03_2
    {
        public static void Run()
        {
            string[] inputFromTXT = System.IO.File.ReadAllLines("input.txt");

            int length = inputFromTXT[0].Length;
            int height = inputFromTXT.Length;

            int currentHeight = -1;
            int currentLength = 0;

            string[,] world = new string[height, length];

            foreach (string stringRow in inputFromTXT)
            {
                currentLength = 0;
                currentHeight++;

                foreach (char charFromStringRow in stringRow)
                {
                    world[currentHeight, currentLength] = charFromStringRow.ToString();
                    currentLength++;
                }
            }

            // Height minus one because arrays start at 0
            height--;

            int run11 = walk(world, height, length, 1,1);
            int run31 = walk(world, height, length, 3, 1);
            int run51 = walk(world, height, length, 5, 1);
            int run71 = walk(world, height, length, 7, 1);
            int run12 = walk(world, height, length, 1, 2);

            int total = run11*run31*run51*run71*run12;
            Console.WriteLine($"total trees is {total}");
        }

        static int walk(string[,] world, int height, int length, int right, int down)
        {
            int currentHeight = 0;
            int currentLength = 0;

            int treeCounter = 0;

            while (true)
            {

                if (world[currentHeight, currentLength] == "#")
                {
                    treeCounter++;
                }

                if (currentHeight == height)
                {
                    break;
                }

                currentLength = ((currentLength + right) % (length));
                currentHeight += down;
            }

            return treeCounter;
        }
    }
}

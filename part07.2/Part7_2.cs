using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace adventofcode_2020
{
    class Part07_2
    {
        public static void Run()
        {
            string[] inputFromTXT = System.IO.File.ReadAllLines("./part07.2/input.txt");
            Dictionary<string, Dictionary<string, int>> bags = new Dictionary<string, Dictionary<string, int>>();

            // Mainbags with gold
            foreach (string row in inputFromTXT)
            {

                // Strip
                var thisBagAndOtherItsData = row.Split("contain");
                var thisbag = thisBagAndOtherItsData[0].Replace(".", "").TrimEnd().Replace("bags", "bag");

                var otherBags = thisBagAndOtherItsData[1].Split(",");
                Dictionary<string, int> otherBagsDictonary = new Dictionary<string, int>();
                foreach (string bag in otherBags)
                {
                    if (!bag.Contains(" no other bags."))
                    {
                        char amount = bag[1];
                        int number = int.Parse(amount.ToString());
                        string bagName = bag.Substring(3).Replace(".", "").Replace("bags", "bag");
                        otherBagsDictonary.Add(bagName, number);
                    }

                }
                // Add
                bags.Add(thisbag, otherBagsDictonary);

            }
            var total = countNestedBags(bags, bags["shiny gold bag"]);
            Console.WriteLine(total);


            static int countNestedBags(Dictionary<string, Dictionary<string, int>> bags, Dictionary<string, int> currentBag)
            {
                int totalForThisBag = 0; 
                foreach (var childBag in currentBag)
                {
                   totalForThisBag += childBag.Value + childBag.Value * countNestedBags(bags, bags[childBag.Key]);
                }

                return totalForThisBag;
            }


        }
    }

}
﻿using System;

namespace adventofcode_2020
{
    class part03_1
    {
        public static void Run() {
            string[] inputFromTXT = System.IO.File.ReadAllLines("input.txt");

            int length = inputFromTXT[0].Length;
            int height = inputFromTXT.Length;

            int currentHeight = -1;
            int currentLength = 0;

            string[,] world = new string[height, length];

            foreach (string stringRow in inputFromTXT)
            {
                currentLength = 0;
                currentHeight++;

                foreach (char charFromStringRow in stringRow)
                {
                    world[currentHeight, currentLength] = charFromStringRow.ToString();
                    currentLength++;
                }
            }

            walk(world, height - 1, length - 1);
        }

        static void walk(string[,] world, int height, int length)
        {
            int currentHeight = 0;
            int currentLength = 0;

            int treeCounter = 0;

            while (true)
            {

                if (world[currentHeight, currentLength] == "#")
                {
                    treeCounter++;
                    Console.WriteLine($"TREE!! HEIGHT: {currentHeight} + LENGTH {currentLength}");
                }

                if (currentHeight == height)
                {
                    break;
                }

                if ((currentLength + 3) > length)
                {
                    currentLength = ((currentLength + 3) % length) - 1;
                    if(currentLength < 0) {
                        currentLength = 0;
                    }
                }
                else
                {

                    currentLength += 3;
                }

                currentHeight++;
            }


            Console.WriteLine($"total trees is {treeCounter}");
        }
    }
}

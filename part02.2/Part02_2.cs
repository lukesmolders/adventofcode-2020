﻿using System;
using System.Collections.Generic;

namespace adventofcode_2020
{
    class Part02_2
    {
        public static void Run()
        {
            string[] inputFromTXT = System.IO.File.ReadAllLines("input.txt");
            int validCounter = 0;        

            foreach (string item in inputFromTXT)
            {
                string[] seperate = item.Split(" ");
                string[] seperateNumbers = seperate[0].Split("-");
                // Lowest position (always -1 want 0 is not there)
                int lowestPosition = Math.Max(int.Parse(seperateNumbers[0]) - 1, 0);
                // Highest position - 1 (because humans count weird)
                int highestPosition = int.Parse(seperateNumbers[1]) - 1;
                // Letter
                char letter = char.Parse(seperate[1].Split(":")[0]);
                // Make every letter its own charArray entry
                char[] password = seperate[2].ToCharArray();
                // IF one XOR the other is true NOT both, add to validCounter
                if (password[lowestPosition] == letter ^ password[highestPosition] == letter) validCounter++;
            }

            Console.WriteLine($"the amount of valid passwords are : {validCounter}");
        }
    }
}

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace adventofcode_2020
{
    class Part06_2
    {
        public static void Run()
        {
            string[] inputFromTXT = System.IO.File.ReadAllLines("./part06.2/input.txt");
            int total = 0;

            List<string> answersFromGroup = new List<string>();
            List<char> answerListFromPerson = new List<char>();

            // Check each row
            foreach (string answerFromPerson in inputFromTXT)
            {
                // check if its not a empty row
                if (answerFromPerson.Length > 0)
                {
                    answersFromGroup.Add(answerFromPerson);
                }
                // if its a empty row
                else
                {
                    List<char> matchedForEveyone = answersFromGroup[0].ToList();

                    foreach (string personInput in answersFromGroup)
                    {
                        char[] personArray = personInput.ToCharArray();
                        matchedForEveyone.RemoveAll(x => !personInput.Any(y => y == x));
                    }
                    total += matchedForEveyone.Count();
                    answersFromGroup.Clear();
                }
            }
            Console.WriteLine($"Total answers is: {total}");
        }
    }
}
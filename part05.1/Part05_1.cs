﻿using System;

namespace adventofcode_2020
{
    class Part05_1
    {
        public static void Run()
        {
            string[] inputFromTXT = System.IO.File.ReadAllLines(".\\input.txt");
            int startingNumber = 127;
            int startingSeat = 7;

            int highest = 0;
            foreach (var ticket in inputFromTXT)
            {
                char[] letters = ticket.ToCharArray();
                
                int highestNumber = startingNumber;
                int lowestNumber = 0;
                int highestSeat = startingSeat;
                int lowestSeat = 0;

                foreach (char letter in ticket)
                {
                    switch (letter)
                    {
                        //lower
                        case 'F':
                        highestNumber = ((highestNumber + lowestNumber) / 2);
                        break;
                        //higher
                        case 'B':
                        lowestNumber = ((highestNumber + lowestNumber) / 2) + 1;
                        break;
                        //right - upper
                        case 'R':
                        lowestSeat = (highestSeat + lowestSeat) / 2 + 1;
                        break;
                        //left - lower
                        case 'L':
                        highestSeat = ((highestSeat + lowestSeat) / 2);
                        break;
                        //default
                        default:
                        break;
                    }

                }

                int seatPosition = (highestNumber * (startingSeat + 1)) + highestSeat;

                if (seatPosition > highest) highest = seatPosition;


            }
                Console.WriteLine(highest);

        }
    }
}

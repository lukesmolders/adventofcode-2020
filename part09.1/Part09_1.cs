using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace adventofcode_2020
{
    class Part09_1
    {
        public static void Run()
        {
            string[] inputFromTXT = System.IO.File.ReadAllLines("./part09.1/input.txt");
            List<Int64> intList = new List<Int64>();


            foreach (string number in inputFromTXT)
            {
                intList.Add(Int64.Parse(number));
            }

            int stepLength = 5;
            int currentPosition = 0;
            bool found = false;;
            List<Int64> rangeFromlist = intList.GetRange(currentPosition, (currentPosition + stepLength));

        foreach (Int64 item in intList)
        {
            found = false;
            rangeFromlist.Clear();
            rangeFromlist = intList.GetRange(currentPosition, stepLength);
            Int64 combinationWeAreLookingFor = intList[currentPosition + stepLength];

            foreach (Int64 itemFromList in rangeFromlist)
            {
                foreach (Int64 secondItemFromList in rangeFromlist)
                {
                    if(itemFromList != secondItemFromList) {
                        found = (itemFromList + secondItemFromList) == combinationWeAreLookingFor;
                    }

                     if(found) {
                        break;
                    }
                }

                if(found) break;
            }

            if(!found) {
                    Console.WriteLine($"First missing number is {combinationWeAreLookingFor}"); 
                    break;
            }

            currentPosition++;
            
        }

        }



    } 

}